﻿using System;
using System.Threading;
using static System.Math;

namespace Task_3
{
    internal class Polynomial
    {
        public Polynomial(int power, params double[] coefficients)
        {
            if ((coefficients != null) && (coefficients[0] != 0))
            {
                this.coefficients = coefficients;
                this.power = power;
            }

            else throw new Exception("First coefficient cannot be a zero !");
        }

        private readonly int power;
        private readonly double[] coefficients;

        public static Polynomial operator +(Polynomial left, Polynomial right)
        {
            var outPower = Max(left.power, right.power);
            var minPower = Min(left.power, right.power);
            var outCoeffSize = Max(left.coefficients.Length, right.coefficients.Length);
            var outCoefficients = new double[outCoeffSize];
            outCoefficients[0] = 1;

            var Out = new Polynomial(outPower, outCoefficients);

            for (int i = 0; i < minPower; i++)
            {
                Out.coefficients[i] = left.coefficients[i] + right.coefficients[i];
            }

            return Out;
        }

        public static Polynomial operator -(Polynomial left, Polynomial right)
        {
            var outPower = Max(left.power, right.power);
            var minPower = Min(left.power, right.power);
            var outCoeffSize = Max(left.coefficients.Length, right.coefficients.Length);
            var outCoefficients = new double[outCoeffSize];
            outCoefficients[0] = 1;

            var Out = new Polynomial(outPower, outCoefficients);

            for (int i = 0; i < minPower; i++)
            {
                Out.coefficients[i] = left.coefficients[i] - right.coefficients[i];
            }

            return Out;
        }

        public static Polynomial operator *(Polynomial left, Polynomial right)
        {
            var outPower = Max(left.power, right.power);
            var minPower = Min(left.power, right.power);
            var outCoeffSize = Max(left.coefficients.Length, right.coefficients.Length);
            var outCoefficients = new double[outCoeffSize];
            outCoefficients[0] = 1;

            var Out = new Polynomial(outPower, outCoefficients);

            for (int i = 0; i < minPower; i++)
            {
                Out.coefficients[i] = left.coefficients[i] * right.coefficients[i];
            }

            return Out;
        }

        public double Сalculate(double argument)
        {
            double result = 0;
            int startPoint = 0;

            for (int i = power; i > 0; i--)
            {
                if (startPoint < coefficients.Length)
                {
                    result += Pow(argument, i) * coefficients[startPoint];
                    startPoint++;
                }
            }

            return result;
        }

        public void Show()
        {
            var tempPower = power;
            Console.ForegroundColor = ConsoleColor.Red;

            for (int i = 0; i < coefficients.Length - 1; i++)
            {
                if ((tempPower > 1) && (coefficients.Length > 0))
                {
                    tempPower--;

                    if (coefficients[i] == 1)
                    {
                        if (coefficients[i + 1] < 0)
                            Console.Write("X" + "^" + (power - i) + " - ");

                        else if (coefficients[i + 1] > 0)
                            Console.Write("X" + "^" + (power - i) + " + ");

                        else if (coefficients[i + 1] == 0)
                            Console.Write("X" + "^" + (power - i) + " + ");
                    }

                    else if (coefficients[i] != 0)
                    {
                        if (coefficients[i + 1] < 0)
                            Console.Write(Abs(coefficients[i]) + "X" + "^" + (power - i) + " - ");

                        else if (coefficients[i + 1] > 0)
                            Console.Write(Abs(coefficients[i]) + "X" + "^" + (power - i) + " + ");

                        else if (coefficients[i + 1] == 0)
                            Console.Write(Abs(coefficients[i]) + "X" + "^" + (power - i) + " + ");
                    }
                }
            }

            if (tempPower == 1)
                Console.WriteLine(coefficients[coefficients.Length - 1] + "X");

            else if ((tempPower <= coefficients.Length) && (tempPower != 1))
                Console.WriteLine(coefficients[coefficients.Length - 1] + "X" + "^" + (power - coefficients.Length + 1));

            else
            {
                Console.Write(coefficients[coefficients.Length - 1]);
                Console.WriteLine("X" + "^" + (power - coefficients.Length));
            }

            Console.ForegroundColor=ConsoleColor.Gray;
            Thread.Sleep(100);
        }
    }
}
