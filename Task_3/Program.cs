﻿using System;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(45, 10);

            var polynomialA = new Polynomial(4, 5, 4, 6, 4);
            var polynomialB = new Polynomial(4, 2, 3, 3, 2);

            Console.Write("Polynomial A: ");
            polynomialA.Show();

            Console.Write("Polynomial B: ");
            polynomialB.Show();

            Console.WriteLine(new string('-',50));

            var polynomialC = polynomialA + polynomialB;
            Console.Write("C = A + B:    ");
            polynomialC.Show();

            polynomialC = polynomialA - polynomialB;
            Console.Write("C = A - B:    ");
            polynomialC.Show();

            polynomialC = polynomialA * polynomialB;
            Console.Write("C = A * B:    ");
            polynomialC.Show();

            Console.WriteLine(new string('-', 50));
            Console.Write("Calculated polynomial C where (X=2): ");
            Console.WriteLine(polynomialC.Сalculate(2));
            
            Console.ReadKey();
        }
    }
}
